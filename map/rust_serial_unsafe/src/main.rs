use std::cmp::min;
use std::time::Instant;
use tracing::{info, span, Level};
use tracing_subscriber::fmt::format::FmtSpan;
use tracing_subscriber::FmtSubscriber;

fn map<U: Send + Sync, V: Send, F: Fn(&U) -> V + Send + Sync>(from: &[U], to: &mut [V], kernel: F) {
    let _span = span!(Level::TRACE, "serial map").entered();
    let length = min(from.len(), to.len());
    for i in 0..length {
        let element = unsafe { to.get_unchecked_mut(i) };
        *element = kernel(unsafe { &from.get_unchecked(i) });
    }
}

#[tracing::instrument]
fn main() {
    println!("Hello, world!");
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .with_span_events(FmtSpan::CLOSE)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");
    info!("Tracing set-up");
    const LENGTH: usize = 300_000_000;
    let mut from = Vec::with_capacity(LENGTH);
    let mut to = Vec::with_capacity(LENGTH);
    for i in 1..=LENGTH {
        from.push(i as f32);
        to.push(0.0);
    }
    let start_map = Instant::now();
    map(&from, &mut to, |u| u.sqrt());
    let elapsed_map = start_map.elapsed();
    info!("serial map executed in {:?}", elapsed_map);

    for (i, v) in to.iter().enumerate() {
        let i_float = (i + 1) as f32;
        let value = i_float.sqrt();
        assert_eq!(&value, v);
    }
}
