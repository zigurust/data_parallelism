const std = @import("std");
const Thread = std.Thread;
const trace = @import("trace");
const Span = trace.Span;

pub const enable_trace = true;

inline fn loopInThread(comptime U: type, comptime V: type, from: []const U, to: []V, kernel: fn (u: U) callconv(.Inline) V) void {
    const span = Span.open("loopInThread");
    defer span.close();
    var i: usize = 0;
    var len = @min(from.len, to.len);
    while (i < len) : (i += 1) {
        to[i] = kernel(from[i]);
    }
}

inline fn map(comptime number_of_threads: usize, comptime U: type, comptime V: type, from: []const U, to: []V, kernel: fn (u: U) callconv(.Inline) V) !void {
    const span = Span.open("map");
    defer span.close();
    const len = @min(from.len, to.len);
    const len_per_thread = len / number_of_threads;
    var threads: [number_of_threads]Thread = undefined;
    for (threads) |*thread_handle, j| {
        const thread = try Thread.spawn(.{}, loopInThread, .{ U, V, from[j * len_per_thread .. (j + 1) * len_per_thread], to[j * len_per_thread .. (j + 1) * len_per_thread], kernel });
        thread_handle.* = thread;
    }
    // join all threads
    // Could be made more effective
    for (threads) |thread| {
        thread.join();
    }
}

inline fn testKernel(u: f32) f32 {
    return @sqrt(u);
}

pub fn main() !void {
    std.debug.print("Hello\n", .{});
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    const len = 300_000_000;
    var from = try std.ArrayList(f32).initCapacity(allocator, len);
    defer from.deinit();
    var to = try std.ArrayList(f32).initCapacity(allocator, len);
    defer to.deinit();
    var i: usize = 1;
    while (i <= len) : (i += 1) {
        try from.append(@intToFloat(f32, i));
        try to.append(0.0);
    }

    try map(4, f32, f32, from.items, to.items, testKernel);

    var success = true;
    for (to.items) |value, j| {
        const iFloat = @intToFloat(f32, j + 1);
        const expectedValue = testKernel(iFloat);
        if (value != expectedValue) {
            success = false;
            std.debug.print("Value is not expected value: {} != {}", .{ value, expectedValue });
        }
    }
    if (success) {
        std.debug.print("Mapping worked\n", .{});
    } else {
        std.debug.print("There was an error regarding the mapping.\n", .{});
    }
}
