use rayon::prelude::*;
use std::time::Instant;
use tracing::{info, span, Level};
use tracing_subscriber::fmt::format::FmtSpan;
use tracing_subscriber::FmtSubscriber;

fn map<U: Send + Sync, V: Send, F: Fn(&U) -> V + Send + Sync>(from: &Vec<U>, kernel: F) -> Vec<V> {
    let _span = span!(Level::TRACE, "parallel map").entered();
    from.par_iter().map(kernel).collect::<Vec<V>>()
}

#[tracing::instrument]
fn main() {
    println!("Hello, world!");
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .with_span_events(FmtSpan::CLOSE)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");
    info!("Tracing set-up");
    const LENGTH: usize = 300_000_000;
    let mut from = Vec::with_capacity(LENGTH);
    for i in 1..=LENGTH {
        from.push(i as f32);
    }
    let start_map = Instant::now();
    let to = map(&from, |u| u.sqrt());
    let elapsed_map = start_map.elapsed();
    info!("Rayon parallel map executed in {:?}", elapsed_map);

    for (i, v) in to.iter().enumerate() {
        let i_float = (i + 1) as f32;
        let value = i_float.sqrt();
        assert_eq!(&value, v);
    }
}
