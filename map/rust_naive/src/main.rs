use std::cmp::min;
use std::num::NonZeroUsize;
use std::thread::{available_parallelism, scope};
use std::time::Instant;
use tracing::{info, span, Level};
use tracing_subscriber::fmt::format::FmtSpan;
use tracing_subscriber::FmtSubscriber;

fn map<U: Send + Sync, V: Send, F: Fn(&U) -> V + Send + Sync + Copy>(
    from: &[U],
    to: &mut [V],
    kernel: F,
) {
    let _span = span!(Level::TRACE, "parallel map").entered();
    let length = min(from.len(), to.len());
    // unsafe usage is justified. 1 is a non-zero usize.
    let max_threads = available_parallelism()
        .unwrap_or(unsafe { NonZeroUsize::new_unchecked(1) })
        .get();
    let len_per_thread = length / max_threads;
    let mut rest = to;
    scope(|s| {
        for i in 0..max_threads {
            let from_part = &from[i * len_per_thread..(i + 1) * len_per_thread];
            let (to_part, res) = rest.split_at_mut(len_per_thread);
            rest = res;
            s.spawn(move || {
                let len = min(from_part.len(), to_part.len());
                for i in 0..len {
                    let u = &from_part[i];
                    to_part[i] = kernel(u);
                }
            });
        }
    });
}

#[tracing::instrument]
fn main() {
    println!("Hello, world!");
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .with_span_events(FmtSpan::CLOSE)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");
    info!("Tracing set-up");
    const LENGTH: usize = 300_000_000;
    let mut from = Vec::with_capacity(LENGTH);
    let mut to = Vec::with_capacity(LENGTH);
    for i in 1..=LENGTH {
        from.push(i as f32);
        to.push(0.0);
    }
    let start_map = Instant::now();
    map(&from, &mut to, |u| u.sqrt());
    let elapsed_map = start_map.elapsed();
    info!("naive parallel map executed in {:?}", elapsed_map);

    for (i, v) in to.iter().enumerate() {
        let i_float = (i + 1) as f32;
        let value = i_float.sqrt();
        assert_eq!(&value, v);
    }
}
