const std = @import("std");
const Thread = std.Thread;

fn function(number: u8) u8 {
    const id = Thread.getCurrentId();
    std.debug.print("Thread {d} id={d}.\n", .{ number, id });
    return 1;
}

pub fn main() !void {
    const cpuCount = Thread.getCpuCount() catch 0;
    std.debug.print("CPU count={d}.\n", .{cpuCount});

    const thread1 = try Thread.spawn(.{}, function, .{1});
    const thread2 = try Thread.spawn(.{}, function, .{2});
    const thread3 = try Thread.spawn(.{}, function, .{3});
    const thread4 = try Thread.spawn(.{}, function, .{4});

    const id = Thread.getCurrentId();
    std.debug.print("Main Thread id={d}.\n", .{id});

    thread1.join();
    thread2.join();
    thread3.join();
    thread4.join();
}
