use std::thread;

fn main() {
    let available_parallelism = thread::available_parallelism().unwrap();

    println!("Available parallelism {available_parallelism}");

    let handle1 = thread::spawn(move || {
        let id = thread::current().id();
        println!("Hello from thread {id:?}");
    });

    let handle2 = thread::spawn(move || {
        let id = thread::current().id();
        println!("Hello from thread {id:?}");
    });

    let handle3 = thread::spawn(move || {
        let id = thread::current().id();
        println!("Hello from thread {id:?}");
    });

    let handle4 = thread::spawn(move || {
        let id = thread::current().id();
        println!("Hello from thread {id:?}");
    });

    println!("Hello from main thread.");

    handle1.join().unwrap();
    handle2.join().unwrap();
    handle3.join().unwrap();
    handle4.join().unwrap();

    println!("Thread succesfully executed");
}
