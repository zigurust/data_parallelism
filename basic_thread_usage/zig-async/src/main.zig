const std = @import("std");

fn factorial(n: u64) u64 {
    std.debug.print("Hello from factorial, with thread id={}\n", .{std.Thread.getCurrentId()});
    var value: u64 = 1;
    var i: u64 = 1;
    while (i <= n) {
        value *= i;
        i += 1;
    }
    return value;
}

pub fn main() !void {
    var frame = async factorial(4);
    std.debug.print("Hello from main thread, with id={}\n", .{std.Thread.getCurrentId()});
    const value = await frame;
    std.debug.print("value={}\n", .{value});
}

test "Test factorial" {
    // Arrange
    const n = 4;
    // Act
    const value: u64 = factorial(n);
    // Assert
    try std.testing.expectEqual(value, 24);
}
