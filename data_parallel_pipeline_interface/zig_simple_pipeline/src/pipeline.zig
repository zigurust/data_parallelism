const std = @import("std");

pub fn Pipeline(comptime number_of_threads: usize) type {
    return struct {
        const Self = @This();
        const num_threads = number_of_threads;
        // I use a dummy value here to simplify things
        thread_pool: [num_threads]u32,

        pub fn init() Self {
            var threads: [num_threads]u32 = undefined;
            var i: u32 = 0;
            while (i < num_threads) : (i += 1) {
                threads[i] = i + 1;
            }
            return Self{
                .thread_pool = threads,
            };
        }

        pub fn map(self: Self, comptime U: type, comptime V: type, from: []const U, to: []V, kernel: fn (u: U) V) void {
            const len = @min(from.len, to.len);
            var i: usize = 0;
            while (i < len) : (i += 1) {
                to[i] = kernel(from[i]);
            }
            for (self.thread_pool) |t| {
                std.debug.print("thread = {}\n", .{t});
            }
        }

        pub fn unzip(self: Self, comptime U: type, comptime V1: type, comptime V2: type, from: []const U, to1: []V1, to2: []V2, kernel: fn (u: U) std.meta.Tuple(&.{ V1, V2 })) void {
            for (self.thread_pool) |t| {
                std.debug.print("thread = {}\n", .{t});
            }
            const len = @min(@min(from.len, to1.len), to2.len);
            var i: usize = 0;
            while (i < len) : (i += 1) {
                const tuple = kernel(from[i]);
                to1[i] = tuple.@"0";
                to2[i] = tuple.@"1";
            }
        }

        pub fn reduce(self: Self, comptime U: type, comptime V: type, from: []const U, initial_value: V, kernel: fn (accumulator: V, value: U) V) V {
            for (self.thread_pool) |t| {
                std.debug.print("thread = {}\n", .{t});
            }
            var accumulator = initial_value;
            for (from) |value| {
                accumulator = kernel(accumulator, value);
            }
            return accumulator;
        }

        pub fn splat(self: Self, comptime U: type, to: []U, value: U) void {
            for (self.thread_pool) |t| {
                std.debug.print("thread = {}\n", .{t});
            }
            for (to) |*val| {
                val.* = value;
            }
        }

        // fan-out? Something necessary for fft?

        pub fn zip(self: Self, comptime U1: type, comptime U2: type, comptime V: type, from1: []const U1, from2: []const U2, to: []V, kernel: fn (u1: U1, u2: U2) V) void {
            for (self.thread_pool) |t| {
                std.debug.print("thread = {}\n", .{t});
            }
            const len = @min(@min(from1.len, from2.len), to.len);
            var i: usize = 0;
            while (i < len) : (i += 1) {
                to[i] = kernel(from1[i], from2[i]);
            }
        }
    };
}
