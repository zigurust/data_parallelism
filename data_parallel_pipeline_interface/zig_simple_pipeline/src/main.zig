const std = @import("std");
const Pipeline = @import("pipeline.zig").Pipeline;

pub fn main() !void {
    std.debug.print("Simple pipeline test\n", .{});

    const Pipeline4 = Pipeline(4);
    const pipeline = Pipeline4.init();
    var from = [_]u32{1} ** 4;
    var to = [_]u32{0} ** 4;
    const Kernels = struct {
        fn addOne(u: u32) u32 {
            return u + 1;
        }
        fn split(u: u32) std.meta.Tuple(&.{ f32, i32 }) {
            const v1 = @intToFloat(f32, u);
            var v2 = @intCast(i32, u);
            v2 -= 5;
            return .{ v1, v2 };
        }
        fn sum(accumulator: u32, value: u32) u32 {
            return accumulator + value;
        }
        fn add(first: u32, second: u32) u32 {
            return first + second;
        }
    };
    pipeline.map(u32, u32, &from, &to, Kernels.addOne);
    const len = to.len;
    var i: usize = 0;
    while (i < len) : (i += 1) {
        std.debug.assert(2 == to[i]);
        try std.testing.expectEqual(@as(u32, 2), to[i]);
    }

    var to1 = [_]f32{0.0} ** 4;
    var to2 = [_]i32{0} ** 4;
    pipeline.unzip(u32, f32, i32, &to, &to1, &to2, Kernels.split);

    for (to1) |value| {
        try std.testing.expectEqual(@as(f32, 2.0), value);
    }
    for (to2) |value| {
        try std.testing.expectEqual(@as(i32, -3), value);
    }

    const reduced_value = pipeline.reduce(u32, u32, &from, 4, Kernels.sum);
    try std.testing.expectEqual(@as(u32, 8), reduced_value);

    pipeline.splat(f32, &to1, 0.0);
    for (to1) |value| {
        try std.testing.expectEqual(@as(f32, 0.0), value);
    }

    var from1 = [_]u32{1} ** 4;
    const from2 = [_]u32{1} ** 4;

    pipeline.zip(u32, u32, u32, &from1, &from2, &from1, Kernels.add);

    for (from1) |value| {
        try std.testing.expectEqual(@as(u32, 2), value);
    }
}
