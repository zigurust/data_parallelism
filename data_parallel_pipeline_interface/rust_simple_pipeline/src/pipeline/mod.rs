use std::iter::zip;

pub struct Pipeline<const N: usize> {
    threads: [u32; N],
}

impl<const N: usize> Pipeline<N> {
    pub fn init() -> Self {
        let mut threads = [0; N];
        for (i, t) in threads.iter_mut().enumerate() {
            *t = i as u32;
        }
        Pipeline { threads }
    }

    pub fn map<U, V, F: Fn(&U) -> V + Send + Sync + Copy>(
        &self,
        from: &[U],
        to: &mut [V],
        kernel: F,
    ) {
        for t in self.threads {
            println!("thread {t}");
        }
        let iter = zip(from, to);

        for (f, t) in iter {
            *t = kernel(f);
        }
    }

    pub fn reduce<U, V, F: Fn(&V, &U) -> V + Send + Sync + Copy>(
        &self,
        from: &[U],
        initial_value: V,
        kernel: F,
    ) -> V {
        for t in self.threads {
            println!("thread {t}");
        }
        let mut accumulator = initial_value;
        for u in from {
            accumulator = kernel(&accumulator, u);
        }
        return accumulator;
    }

    pub fn splat<U: Copy>(&self, to: &mut [U], value: U) {
        for t in self.threads {
            println!("thread {t}");
        }
        for t in to {
            *t = value;
        }
    }

    pub fn zip<U1, U2, V, F: Fn(&U1, &U2) -> V + Send + Sync + Copy>(
        &self,
        from1: &[U1],
        from2: &[U2],
        to: &mut [V],
        kernel: F,
    ) {
        for t in self.threads {
            println!("thread {t}");
        }
        let iter = zip(to, zip(from1, from2));
        for (t, (u1, u2)) in iter {
            *t = kernel(u1, u2);
        }
    }

    pub fn unzip<U, V1, V2, F: Fn(&U) -> (V1, V2) + Send + Sync + Copy>(
        &self,
        from: &[U],
        to1: &mut [V1],
        to2: &mut [V2],
        kernel: F,
    ) {
        for t in self.threads {
            println!("thread {t}");
        }
        let iter = zip(zip(to1, to2), from);
        for ((t1, t2), u) in iter {
            (*t1, *t2) = kernel(u);
        }
    }
}
