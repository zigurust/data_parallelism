use rust_simple_pipeline::pipeline::Pipeline;
use std;

fn main() {
    let pipeline: Pipeline<4> = Pipeline::init();
    let from: [u32; 4] = [2; 4];
    let mut to: [u32; 4] = [0; 4];

    println!("map");
    pipeline.map(&from, &mut to, |u| u * u);
    for t in to {
        assert_eq!(4, t);
    }

    println!("reduce");
    let sum = pipeline.reduce(&to, 16, |accumulator, u| accumulator + u);
    assert_eq!(32, sum);

    println!("splat & reduce");
    pipeline.splat(&mut to, 1);
    let sum = pipeline.reduce(&to, 0, |accumulator, u| accumulator + u);
    assert_eq!(4, sum);

    println!("zip");
    let from2: [u32; 4] = [3; 4];
    pipeline.zip(&from, &from2, &mut to, |u1, u2| u1 + u2);
    for t in to {
        assert_eq!(5, t);
    }

    println!("unzip");
    let mut to2: [u32; 4] = [0; 4];
    pipeline.unzip(&from, &mut to, &mut to2, |u| (2 * u, u / 2));
    for (t1, t2) in std::iter::zip(to, to2) {
        assert_eq!(4, t1);
        assert_eq!(1, t2);
    }
}
