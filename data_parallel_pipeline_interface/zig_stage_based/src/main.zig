const std = @import("std");
const DataParallelPipeline = @import("data_parallel_pipeline.zig");
const NormalStage = DataParallelPipeline.NormalStage;
const UnzippedStage = DataParallelPipeline.UnzippedStage;
const ExecutionEnvironment = DataParallelPipeline.ExecutionEnvironment;

inline fn mapToU32(u: f32) u32 {
    return @bitCast(u32, u);
}

inline fn unzip(u: u32) std.meta.Tuple(&.{ f32, u32 }) {
    return .{ @bitCast(f32, u), u + 1 };
}

inline fn unzipf32(u: f32) std.meta.Tuple(&.{ f32, f32 }) {
    return .{ u - 0.5, u + 0.5 };
}

inline fn zero(u: u32) u32 {
    _ = u;
    return 0;
}

pub fn main() !void {
    std.debug.print("Execute a simple pipeline\n", .{});

    var from = [_]f32{ 0.0, 1.0, 2.0, 3.0 };

    const stage_1 = DataParallelPipeline.open(4, f32, &from);

    var to = [_]u32{0} ** 4;

    const stage_2 = stage_1.map(u32, &to, mapToU32);

    for (stage_2.from) |value, i| {
        std.debug.print("{} = {}\n", .{ i, value });
    }

    std.debug.print("Simple pipeline executed\n", .{});
}
