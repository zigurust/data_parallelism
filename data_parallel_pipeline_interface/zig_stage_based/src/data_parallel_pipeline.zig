const std = @import("std");
const Tuple = std.meta.Tuple;

pub const ExecutionEnvironment = struct {
    number_of_threads: usize,
};

pub fn open(comptime number_of_threads: usize, comptime U: type, from: []U) NormalStage(U) {
    var execution_environment = ExecutionEnvironment{
        .number_of_threads = number_of_threads,
    };
    const FirstStage = NormalStage(U);
    const first_stage = FirstStage{
        .from = from,
        .execution_environment = execution_environment,
    };

    return first_stage;
}

pub fn NormalStage(comptime U: type) type {
    return struct {
        const Self = @This();
        from: []U,
        execution_environment: ExecutionEnvironment,
        pub fn map(self: Self, comptime V: type, to: []V, kernel: fn (u: U) callconv(.Inline) V) NormalStage(V) {
            const len = @min(self.from.len, to.len);
            var i: usize = 0;
            while (i < len) : (i += 1) {
                to[i] = kernel(self.from[i]);
            }
            const NextStage = NormalStage(V);
            const next_stage = NextStage{
                .from = to,
                .execution_environment = self.execution_environment,
            };
            return next_stage;
        }

        pub fn unzip(self: Self, comptime V1: type, comptime V2: type, to1: []V1, to2: []V2, kernel: fn (u: U) callconv(.Inline) Tuple(&.{ V1, V2 })) UnzippedStage(V1, V2) {
            const len = @min(@min(self.from.len, to1.len), to2.len);
            var i: usize = 0;
            while (i < len) : (i += 1) {
                const tuple = kernel(self.from[i]);
                to1[i] = tuple.@"0";
                to2[i] = tuple.@"1";
            }
            const NextStage = UnzippedStage(V1, V2);
            const next_stage = NextStage{
                .from1 = to1,
                .from2 = to2,
                .execution_environment = self.execution_environment,
            };
            return next_stage;
        }
    };
}

pub fn UnzippedStage(comptime U: type, comptime V: type) type {
    return struct {
        const Self = @This();
        from1: []U,
        from2: []V,
        execution_environment: ExecutionEnvironment,

        pub fn zip(self: Self, comptime W: type, to: []W, kernel: fn (u: U, v: V) callconv(.Inline) W) NormalStage(W) {
            const len = @min(@min(self.from1.len, self.from2.len), to.len);
            var i: usize = 0;
            while (i < len) : (i += 1) {
                to[i] = kernel(self.from1[i], self.from2[i]);
            }
            const NextStage = NormalStage(V);
            const next_stage = NextStage{
                .from = to,
                .execution_environment = self.execution_environment,
            };
            return next_stage;
        }
    };
}
