pub fn map(comptime U: type, comptime V: type, comptime N: usize, src: *const [N]U, dest: *[N]V, kernel: fn (u: *const U) V) !void {
    const task_fn = struct {
        fn func(u_raw: *const void, v_raw: *void) void {
            const u: *const U = @ptrCast(@alignCast(u_raw));
            const v = kernel(u);
            const v_ptr: *V = @ptrCast(@alignCast(v_raw));
            v_ptr.* = v;
        }
    }.func;
    var task = WorkerTask{
        .task_fn = task_fn,
        .u = @ptrCast(&src[0]),
        .v = @ptrCast(&dest[0]),
    };

    task.execute();
}

fn worker_function() void {
    // checks if work queue is not empty
}

const WorkerTask = struct {
    task_fn: *const fn (u: *const void, v: *void) void,
    u: *const void,
    v: *void,
    fn execute(self: *WorkerTask) void {
        self.task_fn(self.u, self.v);
    }
};

test "minimal map" {
    // Arrange
    const src: [1]u32 = [_]u32{2};
    var dest: [1]u32 = [_]u32{0};
    const kernel = struct {
        fn kernel(u: *const u32) u32 {
            return u.* + 1;
        }
    }.kernel;
    const expectEqual = @import("std").testing.expectEqual;
    // Act
    try map(u32, u32, 1, &src, &dest, kernel);
    // Assert
    try expectEqual(3, dest[0]);
}

test "minimal map u8" {
    // Arrange
    const src: [1]u8 = [_]u8{3};
    var dest: [1]u8 = [_]u8{0};
    const kernel = struct {
        fn kernel(u: *const u8) u8 {
            return u.* * u.*;
        }
    }.kernel;
    const expectEqual = @import("std").testing.expectEqual;
    // Act
    try map(u8, u8, 1, &src, &dest, kernel);
    // Assert
    try expectEqual(9, dest[0]);
}
