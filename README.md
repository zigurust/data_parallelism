# Readme

* Sample and example code for data parallelism concepts explored in the ZigURust blog
* License is MIT, see [LICENSE](.LICSENSE)
* Examples are usually executed with `cargo run` or `zig run`

